package model;

import java.io.Serializable;

/**
 * Created by Administrador on 03/04/2016.
 */
public class User implements Serializable {

    private static final long serialVersionUID = -1311207652418170309L;

    private int id;
    private String userName;
    private String password;
    private String address;
    private String email;

    public User() {
    }

    public User(int id, String userName, String password, String address, String email) {
        this.id = id;
        this.userName = userName;
        this.password = password;
        this.address = address;
        this.email = email;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
