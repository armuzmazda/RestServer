package db;

import model.User;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Administrador on 03/04/2016.
 */
public class Connect {
    Connection con = null;

    public Connect() throws ClassNotFoundException, IllegalAccessException, InstantiationException, SQLException {
        String url = "jdbc:mysql://localhost:3306/rest";
        String user = "rest";
        String password = "rest";

        Class.forName("com.mysql.jdbc.Driver").newInstance();
        con = DriverManager.getConnection(url, user, password);
    }

    public  void closeConnection() throws SQLException {
        con.close();
    }

    public void insertUser(User user){
        try{
            PreparedStatement statement = con.prepareStatement("INSERT USER (id, user_name, password, address, email) VALUES (?,?,?,?,?)");
            statement.setInt(1, user.getId());
            statement.setString(2, user.getUserName());
            statement.setString(3, user.getPassword());
            statement.setString(4, user.getAddress());
            statement.setString(5, user.getEmail());
            statement.execute();
        } catch (SQLException e) {
            Logger logger = Logger.getLogger(Connect.class.getName());
            logger.log(Level.SEVERE, e.getMessage(), e);
        }
    }

    public User getUser(int id){
        User user = null;
        try{
            PreparedStatement statement = con.prepareStatement("SELECT * FROM USER WHERE id = ?");
            statement.setInt(1, id);

            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next()){
                user = new User();
                user.setId(resultSet.getInt(1));
                user.setUserName(resultSet.getString(2));
                user.setPassword(resultSet.getString(3));
                user.setAddress(resultSet.getString(4));
                user.setEmail(resultSet.getString(5));
            }
        } catch (SQLException e) {
            Logger logger = Logger.getLogger(Connect.class.getName());
            logger.log(Level.SEVERE, e.getMessage(), e);
        }

        return user;
    }

    public void deleteUser(int id){
        try{
            PreparedStatement statement = con.prepareStatement("DELETE FROM USER WHERE id = ?");
            statement.setInt(1, id);
            statement.execute();
        } catch (SQLException e) {
            Logger logger = Logger.getLogger(Connect.class.getName());
            logger.log(Level.SEVERE, e.getMessage(), e);
        }
    }
}
