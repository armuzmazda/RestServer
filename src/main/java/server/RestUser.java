package server;

import db.Connect;
import model.User;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.sql.SQLException;

/**
 * Created by Administrador on 03/04/2016.
 */
@Path("/user")
public class RestUser {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public User getUser(@QueryParam("id")int id)
            throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {
        Connect connect = new Connect();
        User user = connect.getUser(id);
        connect.closeConnection();
        return  user;
    }

    @GET
    @Produces(MediaType.TEXT_HTML)
    public String getUserHTML(@QueryParam("id") int id) throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {
        Connect connect = new Connect();
        User user = connect.getUser(id);
        connect.closeConnection();
        return "<HTML><BODY>" + user.getUserName() + "</BODY></HTML>";
    }

    @PUT
    public Response createUser(User user) throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {
        Connect connect = new Connect();
        connect.insertUser(user);
        connect.closeConnection();
        return Response.status(Response.Status.OK).build();
    }

    @DELETE
    public Response deleteUser(@QueryParam("id")int id) throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {
        Connect connect = new Connect();
        connect.deleteUser(id);
        connect.closeConnection();
        return Response.status(Response.Status.OK).build();
    }
}
